#!/bin/bash
#Jesús Camacho
#
# 13/10/2016
# 0.1v
# podmodular.sh
# Para ejecutar es necesario cambiar los permisos con chmod 755 podmodular.sh. Luego para ejecutarlo: ./podmodular.sh

###       ###
# VARIABLES #
###       ###
dirPodcast=/home/$USER/Música/BirrasYBits
# ex section:section:section:...
listSections=TomasFalsas:Presentacion:JournalD:ElDebate:ArHistoria:LinuxMania:BoletinHack:varspoolmail:devnull:Despedida:TuxATux
numSections=12


###       ###
# FUNCIONES #
###       ###
# Existe dir
function dirExist {
	if [ -d $1 ]
	then
		return 0
	else
		return 1
	fi
}

function interactiveHelp {
	echo $1
	echo "
	Forma de uso: NOMBRESCRIPT [OPCIONES]
	Opciones				Significado
	-h 						Muestra este mensaje
	-i						Crear arbol de directorios
	"
}

#Variable Vacia
function varEmpty {
	[ -z $1 ]
}

###           ###
# PROGRAMA BASE #
###           ###

	if varEmpty $1
	then
		interactiveHelp
	else

		case $1 in
		-h)
			interactiveHelp
		;;

		-i)
			if dirExist $dirPodcast
			then
				echo "Directorio $dirPodcast existe"
			else
				mkdir -p $dirPodcast
				echo "Directorio $dirPodcast creado."
			fi

			count=1
			numDir=-1
			while [[ $count -lt $numSections ]]; do
				section=`echo $listSections | cut -d":" -f$count`
				(( numDir++ ))
				if [[ $numDir -eq 10 ]]; then
					echo "Creando... "$numDir"_"$section
					mkdir $dirPodcast/$numDir"_"$section
					mkdir $dirPodcast/$numDir"_"$section/Original
					mkdir $dirPodcast/$numDir"_"$section/SinRuido
				else
					echo "Creando... 0"$numDir"_"$section
					mkdir $dirPodcast/"0"$numDir"_"$section
					mkdir $dirPodcast/"0"$numDir"_"$section/Original
					mkdir $dirPodcast/"0"$numDir"_"$section/SinRuido
				fi
				(( count++ ))
			done
		;;

		*)
			echo "DEFAULT"
		;;
		esac
	fi
